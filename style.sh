#!/bin/bash

# Normalize SVG and create default CSS
./style.sed "$1" > "${1%.svg}-new.svg"

# Create custom-based CSS
sort -u tmp.css > style.css ; cat custom.css >> style.css 
