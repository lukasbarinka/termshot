# TermShot

TermShot is simple CLI tool that normalizes SVG output of XTerm screen-shot. There are several problems using XTerm SVG screen-shot tool:

1. Everything is black (background rectangles)
1. Colors are unusable (uses fill attribute of element instead of fill attribute of style)
1. Numbers in RGB colors contain "," as decimal separator insead of "."

![htop](htop.png)
-- *htop in XTerm - Rendered in Firefox, [download SVG](htop.svg) or [download original SVG from XTerm](htop-orig.svg)*

## Description

This tool solves mentioned problems of SVG from output of XTerm screen-shot. It creates new SVG file where:

- Removes <rect> elements (black background)
- Changes fill attribute into class
- Inserts definition of external CSS (style.css)
- Creates default CSS from SVG values and appends custom style.

## Instalation

Just clone/download repository, e.g.:

```
$ git clone https://gitlab.com/lukasbarinka/termshot.git
```

Project consists of:

* **style.sh**
  * main script (shell)
  * calls `style.sed`, creates `$1-new.svg` file
  * concatenates styles from SVG and `custom.css`, creates `style.css`
* **style.sed**
  * normalizes SVG file (sed)
  * adds `\?xml-stylesheet type="text/css" href="style.css"?>` declaration
  * removes `<rect>` elements
  * converts `fill=...` attributes into `class=...` attributes
  * creates `tmp.css` with fill styles/classes
* **style.css**, **custom.css**
  * CSS examples
* **htop-orig.svg**, **htop.svg**
  * example SVG files (original and normalized using this script)

## Usage

The `style.sh` script requires SVG file as parameter. The `SVG` file from XTerm screen-shot is expected.

1. Start XTerm program: `xterm`
1. `Ctrl+LMB` > `SVG Screen Dump` (LMB=Left Mouse Button)
1. Copy/move `~/xterm...svg` into project directory
1. Execute `./style.sh` script with SVG filename as parameter
1. See result (e.g.: in Chrome/Firefox/Inkscape)

```
cd termshot
cp ~/xterm.screenshot.svg .
./style.sh xterm.screenshot.svg
firefox xterm.screenshot-new.svg
```

If you want to change text colors, copy `style.css` file into `custom.css` and change colors in class definitions.


## License

Code of this project is licensed under the GNU GENERAL PUBLIC LICENSE, Version 3. See the LICENSE file for details.

![License:GNU/GPL3](https://img.shields.io/badge/license-GNU%2FGPL3-green)
