#!/bin/sed -f


1a <?xml-stylesheet type="text/css" href="style.css"?>
# Inline CSS after <svg>
#/<svg/{
#	:a
#	/>/! { N; ba }
#	/>/ {
#		a <defs>
#		a <style type="text/css"><![CDATA[
#		a   .c8984-8984-8984 { fill: red; }
#      a   .c3605-3605-10000 { fill: blue; }
#		a ]]></style>
#		a </defs>
#	}
#}

# Remove rectangles
/<rect/{d;b;}

# Convert fill color to class
/fill='rgb/{
	# H: 1. Whole line
	h
	s/.*fill='\(rgb([^)]*)\)'.*/\1/
	# Correct decimal , to .
	s/,\([^ ]\)/.\1/g
	H
	# H: 1. Whole line, 2. RGB
	s/, /-/g
	s/[^0-9-]//g
	H
	# H: 1. Whole line, 2. RGB, 3. class-name
	g
	s/\(.*\)\n\(.*\)\n\(.*\)/.c\3 { fill: \2; }/
	# Save CSS file
	w tmp.css
	g
	s/\(.*\)fill='rgb([^)]*)'\(.*\)\n\(.*\)\n\(.*\)/\1class='c\4'\2/
}
